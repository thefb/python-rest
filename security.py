from user import User
from werkzeug.security import safe_str_cmp

# users = [
#     User(1, 'fabs', 'fabs123')
# ]

# username_mapping = {u.username: u for u in users}

# userid_mapping = {u.id: u for u in users}


def authentication(username, password):
    user = User.username_find(username)
    if user and safe_str_cmp(user.password, password):
        return user


def identity(payload):
    user_id = payload['identity']
    return User.id_find(user_id)
