import sqlite3

connection = sqlite3.connect('db.db')

cursor = connection.cursor()

create_table = "CREATE TABLE users (id int, username text, password text)"


# cursor.execute(create_table)

user = (1, 'fabs', 'fabs123')

create_user = "INSERT INTO users VALUES (?, ?, ?)"

# cursor.execute(create_user, user)

users = [
    (2, 'quel', 'quel123'),
    (3, 'joames', 'joames123')
]

cursor.executemany(create_user, users)
connection.commit()

select_query = "SELECT * FROM users"

for row in cursor.execute(select_query):
    print(row)

connection.close()
